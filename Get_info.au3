#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
; #INDEX# =============================================================================================================================
; Titre .........: Get_info																											  =
; Auteur ........: Nicolas Bertrand																									  =
; AutoIt Version : 3.3.14.5																											  =
; Langue ......: Français																											  =
; Description ...: Ce script permet de récupérer la taille de la RAM et du disque dur, de récupérer l'uuid et le nom de l'ordinateur, =
; de savoir si c'est un ssd et de nous indiquer si l'ordinateur est éligible. Toutes ces informations sont ensuite mises dans         =
;un fichier csv ou json.										  																	  =
; =====================================================================================================================================
#include <MemoryConstants.au3>
#include <File.au3>

#Region : Récupération de la taille de la RAM
;Récupère toutes les informations de la mémoire de l'ordinateur
$RAM = MemGetStats()
;Récupère dans une variable la mémoire totale physique de l'ordinateur et le convertit en Gb
$RAMT= $RAM[$MEM_TOTALPHYSRAM]/1024/1024;
;Arrondis la taille récupérer à 0 chiffre après la virgule
$RAMT = Round($RAMT, 0);
#EndRegion

#Region : Vérification du type de lecteur (SSD ou non)
;Récupère le type du lecteur et indique si c'est un SSD ou non
;Si ce n'est pas un SSD il retournera un résultat vide
$ssd = DriveGetType("C:\", 2)
;Vérifie que le résultat retourné dans la variable $ssd est vide
IF ($ssd = "") Then
	;Si elle est vide on indique que ce n'est pas SSD
	$ssd = "Not SSD"
EndIf
#EndRegion

#Region : Récupération de l'uuid de la machine
$classeWMI = ObjGet("winmgmts:\\.\root\cimv2")
;Exécution de la requête pour récupéré les données de la classe Win32_ComputerSystemProduct
$requete = $classeWMI.ExecQuery("Select * From Win32_ComputerSystemProduct", "WQL", 0x30)
;Parcours du tableau contenant le résultat de la requête
For $resultat In $requete
	;Insertion dans une variable de l'uuid trouver dans la requête
    $uuid = $resultat.uuid
Next
#EndRegion

#Region : Récupération de la taille du stockage
;Récupération de la taille du lecteur C: en Mégaoctet
$Disk = DriveSpaceTotal ( "C:\")
;Convertion en Gigaoctect
$Disk = $Disk/1024
;Formatage pour ne prendre que les chiffres avant la virgule
$sizeDisk = StringFormat("%d", $Disk)
#EndRegion

#Region : Vérification des données pour savoir si la machine est éligible
;Si la machine a 3GO ou plus de RAM et qu'elle possède aux moins 80GO ou 64GO pour un SSD alords la variable eligible est à oui sinon elle est à non
If ($RAMT >= 3) AND (($sizeDisk >= 64 AND $ssd = "SSD") OR ($sizeDisk >= 80))  Then
	$eligible = "Oui"
Else
	$eligible = "Non"
EndIf
#EndRegion

#Region : Création d'un fichier au format CSV
;Vérifie si un dossier du nom de CSV existe, si ce n'est pas le cas il le crée
If Not FileExists("CSV") Then
	DirCreate("CSV")
Endif

;Si le fichier InfoOrdi.Csv n'existe pas alords il le crée et ajout les informations sinon il ouvre juste le fichier et ajoute à la suite les informations
If Not FileExists("CSV\InfoOrdi.csv") Then
	;Crée et ouvre le fichier InfoOrdi.csv avec les permission d'écriture
	Global $Info = FileOpen(@ScriptDir & "\CSV\InfoOrdi.csv", 1)
	;Ecrit la légende dans le fichier csv
	FileWrite($Info, "Nom de l'ordinateur;UUID;Taille du stockage(GB);Taille de la RAM(GB);Type de stockage;Eligible ?"&@CRLF)
	;Écriture de toutes les informations récupérées dans le fichier csv crée précédemment
	FileWrite($Info, @ComputerName & ";" & $uuid & ";" & $sizeDisk & ";" &$RAMT & ";" & $ssd & ";" & $eligible)
	;Fermeture du fichier InfoOrdi.csv
	FileClose($Info)
Else
	;Ouvre le fichier InfoOrdi.csv avec les permission d'écriture
	Global $Info = FileOpen(@ScriptDir & "\CSV\InfoOrdi.csv", 1)
	;Écriture de toutes les informations récupérées dans le fichier csv ouvert précédemment
	FileWrite($Info, @LF &@ComputerName & ";" & $uuid & ";" & $sizeDisk & ";" &$RAMT & ";" & $ssd & ";" & $eligible)
	;Fermeture du fichier InfoOrdi.csv
	FileClose($Info)
EndIf
#EndRegion

#Region : Création d'un fichier au format JSON
;Vérifie si un dossier du nom de JSON existe, si ce n'est pas le cas il le crée
If Not FileExists("JSON") Then
	DirCreate("JSON")
Endif

;Définit l'emplacement du json
$emplacement = @ScriptDir & "\JSON\InfoOrdi.json"

;Si le fichier Info Ordi.json n'existe pas alords il le crée et ajout les informations sinon il ouvre juste le fichier et ajoute à la suite les informations
If Not FileExists("JSON\InfoOrdi.json") Then
	;Création et ouverture du fichier InfoOrdi.json
	$json = FileOpen($emplacement, 1)
	;Création d'un tableau où l'on insère les éléments obtenus précédemment au format json
	Dim $tableau_info[10]=['[',' {','   "Nom_PC" : "' & @ComputerName & '",','   "UUID" : "' & $uuid & '",','   "Taille_Stockage(GB)" : "' & $sizeDisk & '",','   "RAM(GB)" : "' & $RAMT & '",','   "Type_stockage" : "' & $ssd & '",','   "Eligible" : "' & $eligible & '"',' }',']']
	;Parcours du tableau pour ajouter chaque ligne du tableau dans le fichier InfoOrdi.json
	For $i = 0 To Ubound($tableau_info) - 1
		FileWrite($json, $tableau_info[$i] & @LF)
	Next
	;Fermeture du fichier InfoOrdi.json
	FileClose($json)
Else
	;Ouverture du fichier InfoOrdi.json
	$json = FileOpen($emplacement, 1)
	;Création d'un tableau où l'on insère les éléments obtenus précédemment au format json
	Dim $tableau_add[8]=[' {','   "Nom_PC" : "' & @ComputerName & '",','   "UUID" : "' & $uuid & '",','   "Taille_Stockage(GB)" : "' & $sizeDisk & '",','   "RAM(GB)" : "' & $RAMT & '",','   "Type_stockage" : "' & $ssd & '",','   "Eligible" : "' & $eligible & '"',' },']
	;Variable pour savoir où ajouter les lignes
	$Ligne = 2
	;Parcours du tableau pour ajouter chaque ligne du tableau dans le fichier InfoOrdi.json a partir de deuxième ligne pour rester dans les crochet
	For $j = 0 To Ubound($tableau_add) - 1
		_FileWriteToLine($emplacement,$Ligne,$tableau_add[$j], False)
		;rajoute plus un à la variable Ligne pour pouvoir ajouter la ligne suivante du tableau en dessous de le-ci rajouter avant
		$Ligne = $Ligne + 1
	Next
	;Fermeture du fichier InfoOrdi.json
	FileClose($json)
EndIf
#EndRegion
Exit
