$CurrentDir = [System.IO.Path]::GetDirectoryName($myInvocation.MyCommand.Definition)
Set-Location $CurrentDir;
Import-Module ".\DriverManagement.psm1";
Get-DriverList -DriverFamily All | Where-Object { $_ -Notmatch "Kaspersky Lab"} | Where-Object { $_ -Notmatch "VirtualBox"} | Backup-Drivers -Repository "C:\tmpdrv\" -Action Replace;
Set-ExecutionPolicy RemoteSigned;