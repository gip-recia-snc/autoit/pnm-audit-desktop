﻿<# 
 .Synopsis
  

 .Description
  

 .Example
   # Get a Collection of all drivers used by the computer
   Get-driverList -DriverFamily All

   # Get a Collection of all non-microsoft drivers used by the computer
   Get-driverList -DriverFamily Tiers

 .Example
   # Backup Tiers Drivers used by the computer. If an existing backup already exists for this model, replace it
   Backup-Drivers -DriverList $(Get-driverList -DriverFamily Tiers) -DriverStore "C:\DriverStore" -Action replace

   # Backup Tiers Drivers used by the computer in Append Mode. Add drivers that are not already backed up or if it's e newer version. 
   Backup-Drivers -DriverList $(Get-driverList -DriverFamily Tiers) -DriverStore "C:\DriverStore" -Action Append

#>

Function Get-OsVersion() {
    switch -Wildcard ((Gwmi -Class Win32_OperatingSystem).Version)
    {
        "6.3*" {return "Windows 8"}
        "6.2*" {return "Windows 8"}
        "6.1*" {return "Windows 7"}
        "6.0*" {return "Windows Vista"}
        "5.2*" {return "Windows XP"}
        "5.0*" {return "Windows 2000"}
    }
}

Function Get-OSArchitecture() {
    if ((gwmi -Class Win32_OperatingSystem).OSArchitecture -like "*64*")
    {
        return "x64"; 
    }
    else
    {
        return "x86"; 
    }
}

Function Get-DriverList() {
    Param
    (
        [Parameter(Mandatory=$true)][ValidateSet("All","Tiers")][String]$DriverFamily
    )
    Process 
    {
        $collection = @();
        $registryEntries = Get-ChildItem HKLM:\System\currentControlSet\Control\Class -Recurse -ErrorAction SilentlyContinue | Get-ItemProperty -Name ProviderName -ErrorAction SilentlyContinue;
        # Clear any registry Access Denied Error entries
        $Error.Clear();
        $i = 0; 
        foreach($item in $registryEntries)
        {
            $properties = @{
                'Class'=(Get-ItemProperty -Name Class -Path $item.PSParentPath).Class;
                'Name'=(Get-ItemProperty -Name DriverDesc -Path $item.PSPath).DriverDesc;
                'ProviderName'=(Get-ItemProperty -Name ProviderName -Path $item.PSPath).ProviderName;
                'DeviceID'=(Get-ItemProperty -Name MatchingDeviceId -Path $item.PSPath).MatchingDeviceId;
                'InfFile'=((Get-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion -Name DevicePath).devicePath + "\" + (Get-ItemProperty -Name infPath -Path $item.PSPath).infPath)};
            
            $iniFile = Get-IniContent -FilePath $properties["InfFile"];
            try
            {
                $properties.Add('Version',$iniFile["Version"]["DriverVer"].split(",")[1].Trim());
            }
            catch 
            {
                Write-Host ("Error while processing `"" + $o.InfFile + "`"") -ForegroundColor Red; 
                $Error | ForEach-Object{Write-Host ("`t" + $_.InnerException.Message) -ForegroundColor Red};
            }
                
                    
            $CatalogFiles = @();
            if ($iniFile["Version"]["CatalogFile"] -ne $null)
            {
                $CatalogFiles += $iniFile["Version"]["CatalogFile"];
            }
            if ($iniFile["Version"]["CatalogFile.NTAMD64"] -ne $null)
            {
                $CatalogFiles += $iniFile["Version"]["CatalogFile.NTAMD64"];
            }
            if ($iniFile["Version"]["CatalogFile.NTx86"] -ne $null)
            {
                $CatalogFiles += $iniFile["Version"]["CatalogFile.NTx86"];
            }
            if ($iniFile["Version"]["CatalogFile.NT"] -ne $null)
            {
                $CatalogFiles += $iniFile["Version"]["CatalogFile.NT"];
            }
            $properties.Add('CatalogFiles',$CatalogFiles);

                    
            $o = New-Object psobject -Property $properties;
            $collection += $o;

            $iniFile = $null;

            $i++;
            Write-Progress -Activity "Gathering Drivers Information" -Status $o.Class -PercentComplete (($i/$registryEntries.Count)*100) -Id 2;
        }

        $collection = $collection | Sort-Object -Property 'InfFile' -Unique
        $collection = $collection | Sort-Object -Property 'Class'

        if ($DriverFamily -eq "Tiers")
        {
            $collection = $collection | Where-Object{$_.ProviderName -ne "Microsoft"}
            $collection = $collection | select Class,ProviderName,Name,Version,infFile,CatalogFiles,DeviceID
            return $collection;
        }
        else 
        {
            $collection = $collection | select Class,ProviderName,Name,Version,infFile,CatalogFiles,DeviceID
            return $collection;
        }
    }
}

Function Get-DeviceIDs() {
    Param
    (
        [Parameter(Mandatory=$true)][ValidateSet("All","Tiers")][String]$DriverFamily
    )
    Process 
    {
        $collection = @();
        foreach ($driver in $(Get-DriverList -DriverFamily $DriverFamily))
        {
            $properties = @{
                'Name'=$driver.Name;
                'ProviderName'=$driver.ProviderName;
                'HardwareID'=$driver.DeviceID;
                'CompatibleIDs'= (gwmi -Class Win32_PnpEntity |Where-Object{$_.HardwareID -eq $driver.DeviceID}).CompatibleID;
            };
            $collection += $(New-Object -TypeName psobject -Property $properties);
        }
    }
    End
    {
        return ,$($collection | select ProviderName,Name,HardwareID,CompatibleIDs);
    }
}

Function Initialize-BackupEnvironnement {
    param(
        [ValidateNotNullOrEmpty()][string]$Repository, 
        [Parameter(Mandatory=$true)][ValidateSet("Replace","Append")][String]$Action
    )
    
    # We start initializing Backup Environment
    $Global:BackupEnvironment = New-Object -TypeName psobject;
    
    # About to open or create Drivers.xml File
    if ((Test-Path ($Repository + "\Control\Drivers.xml")) -eq $true)
    {
        Add-Member -InputObject $Global:BackupEnvironment -MemberType NoteProperty -Name DRXMLFile -Value $([Xml](Get-Content -Path ($Repository + "\Control\Drivers.xml") -Encoding UTF8));
    }
    Else 
    {
        if ((Test-Path -Path ($Repository + "\Control")) -eq $false)
        {
            $Error.Clear(); 
            New-Item -Path ($Repository + "\Control") -ItemType directory -Force | out-null;
            if ($Error.Count -ne 0) 
            {
                Write-Host "Error" -ForegroundColor Red;
                $Error | ForEach-Object{Write-Host ("`t" + $_.InnerException.Message) -ForegroundColor Red};
                Exit 1; 
            }
        }
        
        # We create the XML file with initial structure
        $Global:BackupEnvironment | Add-Member -MemberType NoteProperty -Name DRXMLFile -Value $([xml](New-Object System.Xml.XmlDocument));

        $XMLDeclaration = $Global:BackupEnvironment.DRXMLFile.CreateXmlDeclaration("1.0", "utf-8", $null);  

        [System.XML.XMLElement]$XMLElement = ($Global:BackupEnvironment.DRXMLFile).CreateElement("DriverRepository");
        ($Global:BackupEnvironment.DRXMLFile).AppendChild($XMLElement) | Out-Null;
        ($Global:BackupEnvironment.DRXMLFile).Save(($Repository + "\Control\Drivers.xml"));

        [void](($Global:BackupEnvironment.DRXMLFile).InsertBefore($XMLDeclaration, ($Global:BackupEnvironment.DRXMLFile).DocumentElement)); 
        ($Global:BackupEnvironment.DRXMLFile).Save(($Repository + "\Control\Drivers.xml"));
    }
    
    
    # We first test if a node already exists
    $XPath = ("/DriverRepository/Architecture[@Value='" + $(Get-OSArchitecture) + "']/OperatingSystem[@Value='" + $(Get-OsVersion) + "']/SystemEnclosure[@Model='" + ((gwmi -Class Win32_ComputerSystem).Model).trim() + "'][@Manufacturer='" + ((gwmi -Class Win32_ComputerSystem).Manufacturer).trim() + "']");
    $Node = ($Global:BackupEnvironment.DRXMLFile).SelectSingleNode($XPath);
    if($node -ne $null)
    {
        # Node already exists
        switch ($Action) 
        { 
            "Replace" {
                Write-Host "If a backup for the current system already exists, existing files and Information will be erased" -ForegroundColor Yellow;
                $CurrentNode = $Node;
                
                $BackupFolder = ($Repository.trim() + "\Drivers\" + $CurrentNode.BackupFolder.trim()).trim() -replace '[\t]*', '';
                # Removing the existing Backup Folder
                if (Test-Path $BackupFolder)
                {
                    $Error.Clear();
                    Write-Host "About to remove existing Backup : " -NoNewline;
                    Remove-Item ($BackupFolder) -Recurse -Force -ErrorAction SilentlyContinue | Out-Null; 
                    if ($Error.Count -gt 0)
                    {
                        Write-Host "Error" -ForegroundColor Red;
                        $Error | ForEach-Object{Write-Host ("`t" + $_.InnerException.Message) -ForegroundColor Red};
                        Exit 1; 
                    }
                    Else 
                    {
                        Write-Host "Success" -ForegroundColor Green;
                    }
                }

                # Creating the new Backup Folder 
                $Error.Clear();
                Write-Host "About to Create Backup Folder : " -NoNewline;
                try
                {
                    New-Item -ItemType directory -Path $BackupFolder -Force | Out-Null;
                    $Global:BackupEnvironment | Add-Member -MemberType NoteProperty -Name BackupFolder -Value $BackupFolder;
                }
                Catch
                {
                    Write-Host "Error" -ForegroundColor Red;
                    $Error | Out-Host;
                    $Error | ForEach-Object{Write-Host ("`t" + $_.InnerException.Message) -ForegroundColor Red};
                    Exit 1; 
                }
                Write-Host "Success" -ForegroundColor Green;
                
                
                # Clear node corresponding to the current System in Drivers.xml File
                $Error.Clear();
                Write-Host "About To clear Drivers.xml : " -NoNewline;
                if ($CurrentNode.Driver -ne $null)
                {
                    try
                    {
                        $CurrentNode.Driver | ForEach-Object{[void]($_.ParentNode.RemoveChild($_));}
                        ($Global:BackupEnvironment.DRXMLFile).Save(($Repository + "\Control\Drivers.xml")) | Out-Null;
                    }
                    Catch
                    {
                        Write-Host "Error" -ForegroundColor Red;
                        $Error | ForEach-Object{Write-Host ("`t" + $_.InnerException.Message) -ForegroundColor Red};
                        Exit 1;
                    }                  
                }
                $Global:BackupEnvironment | Add-Member -MemberType NoteProperty -Name CurrentNode -Value $XPath;
                Write-Host "Success" -ForegroundColor Green; 

                Break;
            } 

            "Append" {
                Write-Host "A backup for the current system already exists. About to append Files and information if necessary" -ForegroundColor Yellow;
                $CurrentNode = $Node;

                # Set The current node
                Write-Host "About to set the Current node : " -NoNewline; 
                $Global:BackupEnvironment | Add-Member -MemberType NoteProperty -Name CurrentNode -Value $XPath;
                Write-Host "Success" -ForegroundColor Green;

                # Set the current Backup Folder 
                Write-Host "About to set the Current backup Folder : " -NoNewline; 
                $Global:BackupEnvironment | Add-Member -MemberType NoteProperty -Name BackupFolder -Value ($Repository + "\Drivers" + $CurrentNode.BackupFolder);
                Write-Host "Success" -ForegroundColor Green;

                Break;
            }
        }
    }
    Else 
    {
        # About to build XML Node for the current system
        $XPath = ("/DriverRepository");
        $Node = ($Global:BackupEnvironment.DRXMLFile).SelectSingleNode($XPath);
        if ($Node -eq $null)
        {
            $XMLElement = ($Global:BackupEnvironment.DRXMLFile).CreateElement("DriverRepository");
            $Global:BackupEnvironment | Add-Member -MemberType noteproperty -Name CurrentNode -Value ($Global:BackupEnvironment.DRXMLFile).AppendChild($XMLElement);
            $Global:BackupEnvironment.DRXMLFile.Save(($Repository + "\Control\Drivers.xml"));
        }
        else
        {
            $Global:BackupEnvironment | Add-Member -MemberType noteproperty -Name CurrentNode -Value $node;
        }
        
        
        $Node = $null;
        $XPath = ("/DriverRepository/Architecture[@Value='" + $(Get-OSArchitecture) + "']");
        $Node = $Global:BackupEnvironment.DRXMLFile.SelectSingleNode($XPath);
        if ($Node -eq $null)
        {
            $XMLElement = ($Global:BackupEnvironment.DRXMLFile).CreateElement("Architecture");
            $XMLElement.SetAttribute("Value", $(Get-OSArchitecture));
            $Global:BackupEnvironment.CurrentNode  = ($Global:BackupEnvironment.CurrentNode).AppendChild($XMLElement);
            $Global:BackupEnvironment.DRXMLFile.Save(($Repository + "\Control\Drivers.xml"));
        }
        else
        {
            $Global:BackupEnvironment.CurrentNode = $Node;
        }
        
        
        $node=$null;
        $XPath = ("/DriverRepository/Architecture[@Value='" + $(Get-OSArchitecture) + "']/OperatingSystem[@Value='" + $(Get-OsVersion) + "']");
        $Node = ($Global:BackupEnvironment.DRXMLFile).SelectSingleNode($XPath);
        if ($Node -eq $null)
        {
            $XMLElement = ($Global:BackupEnvironment.DRXMLFile).CreateElement("OperatingSystem");
            $XMLElement.SetAttribute("Value", $(Get-OsVersion));
            $Global:BackupEnvironment.CurrentNode  = ($Global:BackupEnvironment.CurrentNode).AppendChild($XMLElement);
            $Global:BackupEnvironment.DRXMLFile.Save(($Repository + "\Control\Drivers.xml"));
        }
        else
        {
            $Global:BackupEnvironment.CurrentNode = $Node;
        }

        
        $Node = $null;
        $XPath = ("/DriverRepository/Architecture[@Value='" + $(Get-OSArchitecture) + "']/OperatingSystem[@Value='" + $(Get-OsVersion) + "']/SystemEnclosure[@Manufacturer='" + (gwmi -Class Win32_ComputerSystem).Manufacturer + "'][@Model='" + (gwmi -Class Win32_ComputerSystem).Model + "']");
        $Node = ($Global:BackupEnvironment.DRXMLFile).SelectSingleNode($XPath);
        if ($Node -eq $null)
        {
            $XMLElement = ($Global:BackupEnvironment.DRXMLFile).CreateElement("SystemEnclosure");
            $XMLElement.SetAttribute("Manufacturer", ((gwmi -Class Win32_ComputerSystem).Manufacturer).trim());
            $XMLElement.SetAttribute("Model", ((gwmi -Class Win32_ComputerSystem).Model).trim());
            $XMLElement.SetAttribute("BackupFolder", (("\" + (Get-OSArchitecture) + "\" + (Get-OsVersion) + "\" + ((gwmi -Class Win32_ComputerSystem).manufacturer).trim() + " - " + ((gwmi -Class Win32_ComputerSystem).Model).trim()))) ;
            $XMLElement.SetAttribute("Version", "1");
            $Global:BackupEnvironment.CurrentNode  = ($Global:BackupEnvironment.CurrentNode).AppendChild($XMLElement);
            $Global:BackupEnvironment.DRXMLFile.Save(($Repository + "\Control\Drivers.xml"));
        }
        else
        {
            $Global:BackupEnvironment.CurrentNode = $Node;
        }
        
        Write-host $CurrentNode -ForegroundColor Yellow;
        
        $Global:BackupEnvironment.DRXMLFile.Save(($Repository + "\Control\Drivers.xml"));
        $Global:BackupEnvironment.CurrentNode = $XPath;


        # About to build BackupFolder for the current system
        $Error.Clear();
        $targetFolder = ($Repository + "\Drivers\" + (Get-OSArchitecture) + "\" + (Get-OsVersion) + "\" + (gwmi -Class Win32_ComputerSystem).manufacturer + " - " + (gwmi -Class Win32_ComputerSystem).Model);
        Write-Host ("About to Create Backup Folder : ") -NoNewline;
        try
        {
            if (Test-Path $targetFolder)
            {
                Remove-Item -Path $targetFolder -Force -Recurse | Out-Null;
            }
            New-Item -Path $targetFolder -ItemType directory -Force -ErrorAction Continue | Out-Null; 
        }
        Catch 
        {
                Write-Host "Error" -ForegroundColor Red;
                $Error | ForEach-Object{Write-Host ("`t" + $_.InnerException.Message) -ForegroundColor Red};
                Exit 1;
        }
        Write-Host "Success" -ForegroundColor Green;
        
        $Global:BackupEnvironment | Add-Member -MemberType NoteProperty -Name BackupFolder -Value $targetFolder;
    }
}

Function Backup-Drivers() {
    param
    (
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)][System.Management.Automation.PSObject[]]$DriverList,
        [Parameter(Mandatory=$true)][ValidateScript({(Test-Path $_)})][String]$Repository,
        [Parameter(Mandatory=$true)][ValidateSet("Replace","Append")][String]$Action
    )
    Begin {$inputObject = @()}
    Process {$inputObject += $DriverList} 
    End
    {
        # Check Driver Repository syntax 
        if ($Repository.EndsWith("\"))
        {
            $Repository = $Repository.Remove($Repository.Length-1);
        }
        
        # We start initializing Backup Environment
        $Global:BackupEnvironment = New-Object -TypeName psobject;
        Initialize-BackupEnvironnement -Repository $Repository -Action $Action;  

        
        # We Create an XML Element. This one will be used to add information to XML file at the right location
        Write-Host "About to find the current node : " -NoNewline
        try
        {
            $currentNode = ($Global:BackupEnvironment.DRXMLFile).SelectSingleNode($Global:BackupEnvironment.CurrentNode);
        }
        Catch
        {
            Write-Host "Error" -ForegroundColor Red;
            $Error | Out-Host
            $Error | ForEach-Object{Write-Host ("`t" + $_.InnerException.Message) -ForegroundColor Red};
            Exit 1;
        }    
        Write-Host "Success" -ForegroundColor Green;


        # Start backing up drivers depending on $Action choosen
        switch ($Action) {
            "Replace" {
                Write-Host "About to Backup Drivers : ";
                $i = 0;
                foreach ($item in $inputObject) {
                    Write-Host ("`t" + $item.Name + " - ") -NoNewline;
                    # Adding XML Node for the current driver
                    $XMLElement = ($Global:BackupEnvironment.DRXMLFile).CreateElement("Driver");
                    $XMLElement.SetAttribute("Class", $item.Class);
                    $XMLElement.SetAttribute("Name",  $item.Name);
                    $XMLElement.SetAttribute("Version",  $item.Version);
                    $XMLElement.SetAttribute("DeviceID",  $item.DeviceID);

                    # we create Backup folder variables 
                    $newfolder = ($item.Class + " - " + $item.Name + " - " + $item.Version);
                    $Newfolder = $Newfolder -replace '[\x22\x2A\x2F\x3A\x3C\x3E\x3F\x5C\x7C]*', '';
                    $TargetFolder = ($Global:BackupEnvironment.BackupFolder + "\" + $newfolder);

                    $result = Backup-Driver;
                    if ($result -eq "Success")
                    {
                        Write-Host "Success" -NoNewline -ForegroundColor Green;
                        $XMLElement.SetAttribute("BackupFolder", $newfolder);  
                        
                        ($Global:BackupEnvironment.DRXMLFile).SelectSingleNode($Global:BackupEnvironment.CurrentNode).AppendChild($XMLElement) | Out-Null;
                    }
                    else
                    {
                        Write-Host $result -NoNewline -ForegroundColor Red;
                    }
                    
                    $i++;              
                    Write-Progress -Activity "Backup Drivers" -Status $item.Name -PercentComplete (($i / $inputObject.Count)*100) -Id 3;
                    Write-Host "";
                }
                $Global:BackupEnvironment.DRXMLFile.Save(($Repository + "\Control\Drivers.xml"));
            }


            "Append" {
                Write-Host "About to Backup Drivers : ";
                $i = 0;
                foreach ($item in $inputObject) 
                {
                    Write-Host ("`t" + $item.Name + " - ") -NoNewline;
                    # We start searching for an existing driver in the repository
                    
                    $XPath = ($Global:BackupEnvironment.CurrentNode + "/Driver[@DeviceID='" + $item.DeviceID + "']")
                    $Node = ($Global:BackupEnvironment.DRXMLFile).SelectSingleNode($XPath);
                    if ($Node -ne $null) 
                    {
                        # We check if the current item is using a newer driver than the back up
                        if ($item.Version -gt $Node.Version)
                        {
                            Write-Host "Replacing the exiting driver : " -NoNewline -ForegroundColor Yellow;
                            # Removing the exiting driver Backup
                            if (Test-Path ($Global:BackupEnvironment.BackupFolder + "\" + $Node.BackupFolder))
                            {
                                $Error.Clear();
                                Remove-Item -Path ($Global:BackupEnvironment.BackupFolder + "\" + $Node.BackupFolder) -Recurse -Force; 
                                if ($Error.Count -gt 0)
                                {
                                    Write-Host "Error" -ForegroundColor Red;
                                    $Error | ForEach-Object{Write-Host ("`t`t" + $_.InnerException.Message) -ForegroundColor Red};
                                }
                            }
                            
                            # Creating Backup folder variables 
                            $newfolder = ($item.Class + " - " + $item.Name + " - " + $item.Version);
                            $Newfolder = $Newfolder -replace '[\x22\x2A\x2F\x3A\x3C\x3E\x3F\x5C\x7C]', '';
                            $TargetFolder = ($Global:BackupEnvironment.BackupFolder + "\" + $newfolder);

                            # Performing Backup
                            $result = Backup-Driver;
                            if ($result -eq "Success")
                            {
                                Write-Host "Success" -NoNewline -ForegroundColor Green;
                                # Changing the Xml information 
                                $node.Version = $item.Version; 
                                $Node.BackupFolder = $newfolder;
                            }
                            else
                            {
                                Write-Host $result -NoNewline -ForegroundColor Red;
                            }

                            $Global:BackupEnvironment.DRXMLFile.Save(($Repository + "\Control\Drivers.xml"));
                        }
                        else
                        {
                            Write-Host "Nothing to perform" -NoNewline -ForegroundColor Green;
                        }
                    }
                    Else
                    {
                        $currentNode = $Node;
                        Write-Host "Adding a new driver - " -ForegroundColor Yellow -NoNewline;
                        # we create Backup folder variables 
                        $newfolder = ($item.Class + " - " + $item.Name + " - " + $item.Version)
                        $Newfolder = $Newfolder -replace '[\x22\x2A\x2F\x3A\x3C\x3E\x3F\x5C\x7C]', '';
                        $TargetFolder = ($Global:BackupEnvironment.BackupFolder + "\" + $newfolder);
                        
                        # Performing Backup
                        $result = Backup-Driver;
                        if ($result -eq "Success")
                        {
                            Write-Host "Success" -NoNewline -ForegroundColor Green;
                            # Creating a new XML Node
                            $XMLElement = $Global:BackupEnvironment.DRXMLFile.CreateElement("Driver"); 
                            $XMLElement.SetAttribute("Version", $item.Version); 
                            $XMLElement.SetAttribute("Class", $item.Class); 
                            $XMLElement.SetAttribute("Name", $item.Name); 
                            $XMLElement.SetAttribute("DeviceID",  $item.DeviceID);
                            $XMLElement.SetAttribute("BackupFolder", $newfolder); 
                            
                            ($Global:BackupEnvironment.DRXMLFile).SelectSingleNode($Global:BackupEnvironment.CurrentNode).AppendChild($XMLElement) | out-null;
                        }
                        else
                        {
                            Write-Host $result -NoNewline -ForegroundColor Red;
                        }
                        
                        $Global:BackupEnvironment.DRXMLFile.Save(($Repository + "\Control\Drivers.xml"));
                    }
                    
                    Write-Host "";
                    $i++; 
                    Write-Progress -Activity "Backup Drivers" -Status $item.Name -PercentComplete (($i / $inputObject.Count)*100) -Id 3;
                }
            }
        }
    }
} 

Function Backup-Driver {
    if ($item.CatalogFiles.count -eq 0)
    {
        # In this case there isn't any catalog file linked to this driver. 
        # So we look for the .inf file with the good version in the Driver repository.
        # Once found we back up folder
        $files = Get-ChildItem -Path ($env:SystemRoot + "\System32\DriverStore\FileRepository") -Filter (get-item -Path $item.infFile).Name -Recurse;
        if ($files.Count -gt 0)
        {
            if ($files.GetType().BaseType.Tostring() -eq "System.IO.FileSystemInfo")
            {
                # In this case only one file was found 
                # We're going to backup driver folder
                $folderToBackup = Get-FolderToBackup -directory $files.Directory.FullName;
            } 
            else
            {
                # In this case more than 1 catalog file was found
                # So we have to find the catalog file corresponding to the driver version declared in the inf file located in "C:\Windows\inf"
                foreach ($file in $files)
                {
                    $inifile = Get-IniContent -FilePath (Get-ChildItem -Path $file.directory.FullName -Filter *.inf).FullName
                    if (($inifile["Version"]["DriverVer"]).contains($item.Version))
                    {
                        $folderToBackup = Get-FolderToBackup -directory $file.Directory.FullName;
                    }
                }
            }
                
            if ($folderToBackup -ne $null)
            {
                Copy-Item -Path $folderToBackup -Destination $TargetFolder -Recurse -Force -ErrorAction SilentlyContinue;
                return "Success"; 
            }
            Else 
            {
                return "No source folder found"; 
            }    
        }
        else
        {
            return "Unable to find files on this computer"; 
        }
    }
    else
    {
        # In this case there is/are catalog(s) file(s) linked to this driver. 
        # So we look for cat files with the good version in the Driver repository.
        # Once found we back up folder
        foreach ($catalogFile in $item.CatalogFiles)
        {
            $files = Get-ChildItem -Path ($env:SystemRoot + "\System32\DriverStore\FileRepository") -Filter $catalogFile -Recurse -ErrorAction SilentlyContinue;
                    
            if($files -ne $null)
            {
                if ($files.GetType().BaseType.Tostring() -eq "System.IO.FileSystemInfo")
                {
                    # In this case only one file was found 
                    # We're going to backup driver folder
                    $folderToBackup = Get-FolderToBackup -directory $files.Directory.FullName;
                } 
                else
                {
		            # In this case more than 1 catalog file was found
                    # So we have to find the catalog file corresponding to the driver version declared in the inf file located in "C:\Windows\inf"
                    foreach ($file in $files)
                    {
				        $inifile = Get-IniContent -FilePath (Get-ChildItem -Path $file.directory.FullName -Filter *.inf).FullName
                        if (($inifile["Version"]["DriverVer"]).contains($item.Version))
                        {
                            $folderToBackup = Get-FolderToBackup -directory $file.Directory.FullName;
                        }
                    }
                }
                if ($folderToBackup -ne $null)
                {
                    Copy-Item -Path $folderToBackup -Destination $TargetFolder -Recurse -Force -ErrorAction SilentlyContinue;
                    return "Success"; 
                }
                Else 
                {
                    return "No source folder found"; 
                }
            }
        }
    }
}

Function Get-IniContent { 
    [CmdletBinding()] 
    Param( 
        [ValidateNotNullOrEmpty()][ValidateScript({(Test-Path $_)})][Parameter(ValueFromPipeline=$True,Mandatory=$True)][string]$FilePath
    ) 
     
    Begin 
    {
        Write-Verbose "$($MyInvocation.MyCommand.Name): Function started"
    }
    Process 
    { 
        $Error.Clear();
        Write-Verbose "$($MyInvocation.MyCommand.Name): Processing file: $Filepath"
        $file = [system.io.file]::ReadAllText($Filepath);
        $file = $file.TrimEnd()
        while ($file.EndsWith("`t") -or $file.EndsWith(" ")) { $file = $file.remove(($file.Length -1), 1);}
        $file | Out-File ($env:TEMP + "\" + ($FilePath | Split-Path -Leaf)) -force;


        $Error.Clear(); 
        $ini = @{} 
        switch -regex -file ($env:TEMP + "\" + ($FilePath | Split-Path -Leaf))
        { 
            "^\[(.+)\][ \t]*$" # Section 
            {
                $section = [String]$matches[1].Trim();
                $ini[$section] = @{}
                $CommentCount = 0 
            } 
                
            "^(;.*)$" # Comment 
            { 
                if (!($section)) 
                { 
                    $section = "No-Section" 
                    $ini[$section] = @{} 
                } 
                $value = $matches[1].Trim()
                $CommentCount = $CommentCount + 1 
                $name = "Comment" + $CommentCount 
                $ini[$section][$name] = $value;
            }

            "(.+?)[ \t]*=[ \t]*(.*)" # Key 
            {
                if($_ -notlike ';*')
                {
                    if (!($section)) 
                    { 
                        $section = "No-Section" 
                        $ini[$section] = @{} 
                    } 
                    $name = [String]($matches[1]).Trim()
                    $value = [String]($matches[2]).Trim()
                    $value = $value -replace ';.*', "";
                    $ini[$section][$name] = $value.Trim()
                }
            }
        }
        if(test-path ($env:TEMP + "\" + $($FilePath | Split-Path -Leaf))) { Get-Item -Path ($env:TEMP + "\" + $($FilePath | Split-Path -Leaf)) | Remove-Item -Force }
        if ($Error.Count -gt 0)
        {
            
            Write-Host "$MyInvocation.MyCommand.Name" -ForegroundColor Red;
            Write-Host ("Error while processing `"" + $FilePath + "`"") -ForegroundColor Red;
            $Error | ForEach-Object{write-host $_.Exception.message -ForegroundColor Red};
        }
    } 
    End 
    {        
        Return ,$ini;
    } 
}

Function Get-FolderToBackup {
    Param( 
        [ValidateNotNullOrEmpty()][string]$directory
    ) 
    Process 
    {
        $dir = Get-Item $directory
        
        while ($dir.Parent.Name -ne "FileRepository")
        {
            Get-FolderToBackup -directory $dir.Parent 
        }
        return $dir.FullName;
    }
}

Function Import-DriversToMDT{
    param(
    [Parameter(Mandatory=$true)][String]$Repository,
    [Parameter(Mandatory=$true)][String]$MDTDeploymentShare
    )

    Function Finalize {
        cd $env:SystemDrive;
        Remove-Module MicrosoftDeploymentToolkit -ErrorAction SilentlyContinue -Force;
        Remove-PSDrive -Name RepositoryDrive;
    }


    $Error.Clear();
    Write-Host "About to check if MDT is installed : " -NoNewline
    if (((Test-Path ($env:ProgramFiles + "\Microsoft Deployment Toolkit\Bin")) -eq $true) -or ((Test-Path ($env:ProgramW6432 + "\Microsoft Deployment Toolkit\Bin")) -eq $true))
    {
        $MDTModulePath = ($env:ProgramFiles + "\Microsoft Deployment Toolkit\Bin\MicrosoftDeploymentToolkit.psd1");
        if (((Test-Path ($env:ProgramW6432 + "\Microsoft Deployment Toolkit\Bin")) -eq $true) -eq $true)
        {
            $MDTModulePath = ($env:ProgramW6432 + "\Microsoft Deployment Toolkit\Bin\MicrosoftDeploymentToolkit.psd1");
        }
        Write-Host "Success" -ForegroundColor Green; 
    }
    Else 
    {
    
        Write-Host "Failed" -ForegroundColor Red; 
        Write-Host "`tYou cannot run this Cmdlet if Microsoft Deployment Toolkit is not installed on this computer"
        Finalize
        Exit ;
    }


    $Error.Clear();
    Write-Host "About to Import MDT module : " -NoNewline
    Import-Module $MDTModulePath -ErrorAction SilentlyContinue;
    if ($Error.Count -ne 0) {
        Write-Host "Failed" -ForegroundColor Red; 
        Write-Host ("`tUnable to find " + $MDTModulePath) -ForegroundColor red;
        Finalize
        Exit 1;
    }
    Else {
        Write-Host "Success" -ForegroundColor Green; 
    }


    Write-Host "About to connect to Repository : " -NoNewline
    $RepositoryDrive= New-PSDrive -Name RepositoryDrive -PSProvider FileSystem -Root $Repository -ErrorAction SilentlyContinue
    if ($RepositoryDrive -ne $null)
    {
        Write-Host "Success" -ForegroundColor Green; 
    }
    Else 
    {
        Write-Host "Failed" -ForegroundColor Red; 
        Write-Host ("`tUnable to bind a PSdrive to the repository") -ForegroundColor red;
        Finalize
        Exit 1; 
    }


    Write-Host "About to connect to MDT Deployment Share : " -NoNewline
    #$test = New-PSDrive -Name test -PSProvider FileSystem -Root $MDTDeploymentShare -Credential $(Get-Credential -Message "Message" -UserName "MYLAB\Administrator");
    $MDTDeploymentShareDrive = New-PSDrive -Name DS001 -PSProvider MDTProvider -Root $MDTDeploymentShare -ErrorAction Continue;
    if ($MDTDeploymentShareDrive -ne $null)
    {
        Write-Host "Success" -ForegroundColor Green; 
    }
    Else 
    {
        Write-Host "Failed" -ForegroundColor Red; 
        Write-Host ("`tUnable to bind a PSdrive to MDT Deployment share") -ForegroundColor red;
        Finalize
        Exit 1; 
    }
    
    
    Write-Host "Select Drivers to import : " -NoNewline
    $XMLFile = [xml](Get-Content -Path $($RepositoryDrive.Root + "\Control\Drivers.xml") -ErrorAction SilentlyContinue -Encoding UTF8);
    if ($XMLFile -ne $null)
    {
        $XPath = ("/DriverRepository/Architecture/OperatingSystem/SystemEnclosure");
        $Nodes = $XMLFile.SelectNodes($XPath); 
    
        $DriverCatalog = @(); 
        foreach ($item in $Nodes)
        {
            $Properties = @{
                'Architecture'=([System.Xml.XmlElement]$item).ParentNode.ParentNode.Value;
                'OperatingSystem'=([System.Xml.XmlElement]$item).ParentNode.Value;
                'Manufacturer'=([System.Xml.XmlElement]$item).Manufacturer;
                'Model'=([System.Xml.XmlElement]$item).Model;
                'BackupFolder'=([System.Xml.XmlElement]$item).BackupFolder;
            }
            $DriverCatalog += $(New-Object -TypeName psobject -Property $Properties)
        }

        $Selection = $DriverCatalog | select Architecture, OperatingSystem, Manufacturer, Model, BackupFolder  | Out-GridView -OutputMode Multiple -Title "Select drivers to import";
        if ($Selection -eq $null)
        {
            Write-Host "No Driver selected" -ForegroundColor yellow; 
            Finalize
            Exit 0;
        }
        Write-Host "Success" -ForegroundColor Green; 
    }
    Else 
    {
        Write-Host "Failed" -ForegroundColor Red; 
        Write-Host ("`tUnable to find Valid repository Folder") -ForegroundColor red;
        Finalize
        Exit 1; 
    } 
    

    foreach ($DriverSet in $Selection)
    {
        $Error.Clear();
        $DriverPackageName = ($DriverSet.Architecture + " - " + $DriverSet.OperatingSystem + " - " + $DriverSet.Manufacturer + " - " + $DriverSet.Model);

        Write-Host ("About to import " + $DriverPackageName + " : ");
        $targetDir = ("DS001:\out-of-box Drivers\" + $DriverSet.Architecture + "\" + $DriverSet.OperatingSystem + "\" + $DriverSet.Manufacturer + "\" + $Driverset.Model);
        if (-not (Test-Path -Path ("DS001:\out-of-box Drivers\" + $DriverSet.Architecture))){new-item -path ("DS001:\out-of-box Drivers") -enable "True" -Name $DriverSet.Architecture -ItemType "folder" | Out-Null }
        if (-not (Test-Path -Path ("DS001:\out-of-box Drivers\" + $DriverSet.Architecture + "\" + $DriverSet.OperatingSystem))){new-item -path ("DS001:\out-of-box Drivers" + "\" + $DriverSet.Architecture) -enable "True" -Name $DriverSet.OperatingSystem -ItemType "folder" | Out-Null}
        if (-not (Test-Path -Path ("DS001:\out-of-box Drivers\" + $DriverSet.Architecture + "\" + $DriverSet.OperatingSystem + "\" + $DriverSet.Manufacturer))){new-item -path ("DS001:\out-of-box Drivers" + "\" + $DriverSet.Architecture + "\" + $DriverSet.OperatingSystem) -enable "True" -Name $DriverSet.Manufacturer -ItemType "folder" | Out-Null}
        if (-not (Test-Path -Path ("DS001:\out-of-box Drivers\" + $DriverSet.Architecture + "\" + $DriverSet.OperatingSystem + "\" + $DriverSet.Manufacturer + "\" + $Driverset.Model))){new-item -path ("DS001:\out-of-box Drivers" + "\" + $DriverSet.Architecture + "\" + $DriverSet.OperatingSystem + "\" + $DriverSet.Manufacturer) -enable "True" -Name $Driverset.Model -ItemType "folder" | Out-Null}
         
        $Error.Clear();
        Import-MDTDriver -path $targetDir -sourcePath ($Repository + "\Drivers" + $DriverSet.BackupFolder) -ErrorAction SilentlyContinue | Out-Null;
        if ($Error.Count -ne 0) {
            Write-Host "Failed" -ForegroundColor Red; 
            $Error | ForEach-Object{Write-Host ("`t" + $_.InnerException.Message) -ForegroundColor Red};
        }
    }
    Finalize
}

Function Import-DriversToSCCM{
    param(
    [Parameter(Mandatory=$true)][ValidateScript({(Test-Path $_)})][String]$Repository
    )

    Function Finalize {
        cd $env:SystemDrive;
        Remove-PSDrive -Name RepositoryDrive;
    }


    Clear-Host;
    $Error.Clear();


    Write-Host ("About to check repository : ") -NoNewline; 
    if (! ($Repository.StartsWith("\\")))
    {
        
        Write-Host ("`t" + "Repository path must be an UNC path") -ForegroundColor Red; 
        return;
    }

    Write-Host "About to check if SCCM admin console is installed : " -NoNewline
    if ($env:SMS_ADMIN_UI_PATH -ne $null)
    {
        Write-Host "Success" -ForegroundColor Green; 
    }
    Else 
    {
    
        Write-Host "Failed" -ForegroundColor Red; 
        Write-Host "`tYou cannot run this Cmdlet if SCCM console is not installed"
        Finalize
        Exit ;
    }


    Write-Host "About to Import SCCM module : " -NoNewline
    if ((test-path -Path ((Get-Item -Path $env:SMS_ADMIN_UI_PATH).Parent.FullName + "\ConfigurationManager.psd1")) -eq $true)
    {
        Import-Module ((Get-Item -Path $env:SMS_ADMIN_UI_PATH).Parent.FullName + "\ConfigurationManager.psd1");
        Write-Host "Success" -ForegroundColor Green; 
    }
    Else 
    {
        Write-Host "Failed" -ForegroundColor Red; 
        Write-Host ("`tUnable to find " + ((Get-Item -Path $env:SMS_ADMIN_UI_PATH).Parent.FullName + "\ConfigurationManager.psd1")) -ForegroundColor red;
        Finalize
        Exit 1; 
    }


    Write-Host "About to connect to Repository : " -NoNewline
    $RepositoryDrive= New-PSDrive -Name RepositoryDrive -PSProvider FileSystem -Root $Repository -ErrorAction SilentlyContinue
    if ($RepositoryDrive -ne $null)
    {
        Write-Host "Success" -ForegroundColor Green; 
    }
    Else 
    {
        Write-Host "Failed" -ForegroundColor Red; 
        Write-Host ("`tUnable to bind a PSdrive to the repository") -ForegroundColor red;
        Finalize
        Exit 1; 
    }


    Write-Host "About to connect to ConfigMgr Drive : " -NoNewline
    if ((Get-PSDrive -PSProvider CMSite).Name -ne $null)
    {
        $SCCMDrive = (Get-PSDrive -PSProvider CMSite).Name
        Write-Host "Success" -ForegroundColor Green; 
    }
    Else 
    {
        Write-Host "Failed" -ForegroundColor Red; 
        Write-Host ("`tUnable to find ConfigMgr PSdrive on the conputer ") -ForegroundColor red;
        Finalize
        Exit 1;
    }


    Write-Host "Select Drivers to import : " -NoNewline
    $XMLFile = [xml](Get-Content -Path $($RepositoryDrive.Root + "\Control\Drivers.xml") -ErrorAction SilentlyContinue -Encoding UTF8);
    if ($XMLFile -ne $null)
    {
        $XPath = $XPath = ("/DriverRepository/Architecture/OperatingSystem/SystemEnclosure");
        $Nodes = $XMLFile.SelectNodes($XPath); 
    
        $DriverCatalog = @(); 
        foreach ($item in $Nodes)
        {
            $Properties = @{
                'Architecture'=([System.Xml.XmlElement]$item).ParentNode.ParentNode.Value;
                'OperatingSystem'=([System.Xml.XmlElement]$item).ParentNode.Value;
                'Manufacturer'=([System.Xml.XmlElement]$item).Manufacturer;
                'Model'=([System.Xml.XmlElement]$item).Model;
            }
            $DriverCatalog += $(New-Object -TypeName psobject -Property $Properties)
        }

        $Selection = $DriverCatalog | select Architecture, OperatingSystem, Manufacturer, Model  | Out-GridView -OutputMode Multiple -Title "Select drivers to import";
        if ($Selection -eq $null)
        {
            Write-Host "No Driver selected" -ForegroundColor yellow; 
            Finalize
            Exit 0;
        }
        Write-Host "Success" -ForegroundColor Green; 
    }
    Else 
    {
        Write-Host "Failed" -ForegroundColor Red; 
        Write-Host ("`tUnable to find Valid repository Folder") -ForegroundColor red;
        Finalize
        Exit 1; 
    }


    foreach ($DriverSet in $Selection)
    {
        $Error.Clear();
        $DriverPackageName = ($DriverSet.Architecture + " - " + $DriverSet.OperatingSystem + " - " + $DriverSet.Manufacturer + " - " + $DriverSet.Model);

        Write-Host ("About to import " + $DriverPackageName + " : ") -NoNewline;

        Get-ChildItem -Path $($RepositoryDrive.Root + "\DriverPackages\" + $DriverPackageName) -Recurse | Remove-Item -Force -Recurse | Out-Null;
    
    
        # Get DriverPackage Object
        cd ($SCCMDrive +":"); 
        if ((Get-CMDriverPackage -Name $DriverPackageName -ErrorAction SilentlyContinue) -eq $null)
        {
            New-CMDriverPackage -Name $DriverPackageName -Description $DriverPackageName -Path $($RepositoryDrive.Root + "\DriverPackages\" + $DriverPackageName) -PackageSourceType StorageDirect -ErrorAction SilentlyContinue | Out-Null;
            if ($Error.Count -ne 0) 
            {
                Write-Host "Error" -ForegroundColor Red;
                $Error | ForEach-Object{Write-Host ("`t" + $_.InnerException.Message) -ForegroundColor Red};
                Exit 1; 
            }
        }


        # Create Categories to apply to drivers objects
        $CMadministrativeCategories = @();
        $Categories = @($DriverSet.Architecture, $DriverSet.OperatingSystem,$DriverSet.Manufacturer,$DriverSet.Model)
        foreach($Category in $categories) {
            if ((Get-CMCategory -CategoryType DriverCategories -Name $Category) -eq $null)
            {
                $CMadministrativeCategories += $(New-CMCategory -CategoryType DriverCategories -Name $Category)
            }
            else 
            {
                $CMadministrativeCategories += $(Get-CMCategory -CategoryType DriverCategories -Name $Category);
            }
        }
    
        # Retrieve Drivers in Backup Folder
        $XPath = ("/DriverRepository/Architecture[@Value='" + $DriverSet.Architecture + "']/OperatingSystem[@Value='" + $DriverSet.OperatingSystem + "']/SystemEnclosure[@Model='" + $DriverSet.Model + "'][@Manufacturer='" + $DriverSet.Manufacturer + "']");
        $Node = $XMLFile.SelectSingleNode($XPath); 
        Set-Location $env:SystemDrive;
        $Drivers = Get-ChildItem -Path ($RepositoryDrive.Root + '\Drivers' + $Node.BackupFolder) -Recurse -Filter "*.inf" ;


        # Import each drivers in SCCM
        $importedDrivers = @(); 
        cd ($SCCMDrive + ":");
        $i = 0; 
        foreach ($driver in $Drivers)
        {
            $Error.Clear();
            $driverObject = Import-CMDriver -UncFileLocation $driver.FullName -EnableAndAllowInstall $true -DriverPackage $(Get-CMDriverPackage -Name $DriverPackageName) -ImportDuplicateDriverOption AppendCategory -AdministrativeCategory $CMadministrativeCategories -UpdateDistributionPointsforDriverPackage $false -UpdateDistributionPointsforBootImagePackage $false;
            if ($Error.Count -gt 0)
            {
                Write-Host "Error" -ForegroundColor Red;
                $Error | ForEach-Object{Write-Host ("`t" + $_.InnerException.Message) -ForegroundColor Red};
                Finalize; 
                Exit 1;
            }
            else 
            {
                $importedDrivers += $driverObject.LocalizedDisplayName;
            }
            $i++; 
            Write-Progress -Activity "Importing Drivers into SCCM" -PercentComplete (($i/$Drivers.Count)*100) -CurrentOperation $driver.Name;
        }
        if ($Error.Count -eq 0)
        {
            Write-Host ("Success") -ForegroundColor Green;
        }
        Else 
        {
            Write-Host ("Error") -ForegroundColor Red;
        }
    }


    Finalize
    

}

export-modulemember -function Get-DriverList;
export-modulemember -function Backup-Drivers;
export-modulemember -function Import-DriversToMDT;
export-modulemember -function Import-DriversToSCCM;
Export-ModuleMember -Function Get-DeviceIDs;