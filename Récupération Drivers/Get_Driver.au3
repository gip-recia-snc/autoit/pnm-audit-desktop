﻿; #INDEX# =============================================================================================================================
; Titre .........: Get_Driver																										  =
; Auteur ........: Nicolas Bertrand																									  =
; AutoIt Version : 3.3.14.5																											  =
; Langue ......: Français																											  =
; Description ...: Ce script permet de récupérer les drivers et de les mettre sur la clé USB										  =
; =====================================================================================================================================
#include <Array.au3>
#include <File.au3>

;Définit le chemin pour les drivers sur la clé usb
$localisationUSB = @ScriptDir & "\tmpdrv\Drivers\"&@OSVersion
;Définit le constructeur de l'ordinateur
$manufacturer = RegRead("HKEY_LOCAL_MACHINE\HARDWARE\DESCRIPTION\System\BIOS", "SystemManufacturer")
;Définit le model de l'ordinateur
$model = RegRead("HKEY_LOCAL_MACHINE\HARDWARE\DESCRIPTION\System\BIOS", "SystemProductName")
;Définit le nom du dossier des drivers d'un ordinateur
$nameFolder = $manufacturer&" - "&$model
;Variable servant à l'affichage d'un message d'erreur
$test= -1
;Variable définisant que le script est bien passé dans le IF
$existe = 0

;Crée un dossier tmpdrv dans le C:\ ci celui ci n'existe pas
If not FileExists("C:\tmpdrv") Then
	DirCreate("C:\tmpdrv")
EndIf

;Crée un dossier nommé grace à la variable $nameFolder sur la clé USB à l'emplacement indiquée dans la variable $localisationUSB si celui-ci n'existe pas
If not FileExists($localisationUSB) Then
	DirCreate($localisationUSB)
EndIf

;Exécute la récupération des drivers-ci le dossier sur la clé n'existe pas
IF not FileExists($localisationUSB&"\"&$nameFolder) Then
	;Enlèlve la sécuriter pour pouvoir démarrer le script powershell
	ShellExecuteWait("powershell.exe" , "set-executionpolicy Bypass")
	;Lance le scrit powerShell pour démarrer la backup des drivers
	ShellExecuteWait("powershell.exe" , @ScriptDir & "\GetDriver.ps1")
	;Remise de la sécuriter pour le démmarage de script
	ShellExecuteWait("powershell.exe" , "set-executionpolicy Restricted")

	;Localisation du dossier qui à pour nom ce qui est dans la variable $nameFolder
	$localisation = _FileListToArrayRec("C:\tmpdrv",$nameFolder,2,1,0,2)
	;Copie du dossier sur la clé USB
	$test = DirCopy($localisation[1],$localisationUSB&"\"&$nameFolder)
	;Supprimer le dossier tmpdrv de C:\
	DirRemove("C:\tmpdrv",1)
	;Variable existe qui confirme que l'on est passé dans le if
	$existe = 1

Else
	;Retourne un message d'information-ci les drivers pour le modèle est déjà présent sur la clé
	MsgBox(64,"Attention !","La récupération des drivers pour ce modèle a déja était effectuer",30)
EndIf

;Si une erreur s'est produite avertie l'utilisateur que cela à échouer sinon le n'avertit pendant 30 secondes que cela à réussie
If @error Or $test = 0 Then
	MsgBox(16,"Erreur","La récupération des drivers à échouer")
ElseIf $existe = 1 Then
	MsgBox(0,"Récupération Terminer !","La récupération des drivers à reussie !",30)
EndIf
